# <img width="32" src="https://codeberg.org/RaptaG/Zap/raw/branch/main/icon.png"> Zap

<a href="https://modrinth.com/modpack/zap"><img alt="Available on Modrinth" height="40" src="https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/compact/available/modrinth_vector.svg"></a>

## 👥 Credits

Thanks to:

- [robotkoer](https://modrinth.com/user/robotkoer), for making the amazing Fabulously Optimized modpack
- **Every single developer** of all the mods included in Zap
- [comp500](https://modrinth.com/user/comp500), for creating the amazing [packwiz](https://packwiz.infra.link) tool, which I use to maintain Zap
- [Modrinth](https://modrinth.com), for making this pack possible to be published to a greater Minecraft modding audience
- [Codeberg](https://codeberg.org), for hosting an open source, [Forgejo](https://forgejo.org) based git service where I host Zap

## 📜 License

This modpack is open source and licensed under the GNU LGPLv3 License.

The icon of Zap is under the [Open Art License](https://three.org/openart/license/index.html), version 1.0
